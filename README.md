### Cached tree task

#### Requirements

* Java 8

#### Build & Run

##### Windows

```
gradlew.bat bootRun
```

##### Linux

```
./gradlew bootRun
```

#### Default links

* [UI](http://localhost:8080/)
* [Swagger with server API](http://localhost:8080/swagger-ui/index.html#/)