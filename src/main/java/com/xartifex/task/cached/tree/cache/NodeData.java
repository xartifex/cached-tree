package com.xartifex.task.cached.tree.cache;

import com.xartifex.task.cached.tree.common.Node;
import com.xartifex.task.cached.tree.db.DbNode;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;


@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class NodeData implements Node {

  private final Set<Node> children = new LinkedHashSet<>();
  private int id;
  private String value;
  private Integer parentId;
  private NodeData parent;
  private boolean deleted;

  public NodeData(DbNode dbNode) {
    id = dbNode.getId();
    value = dbNode.getValue();
    Node _parent = dbNode.getParent();
    if (_parent != null) {
      parentId = dbNode.getParent().getId();
    }
    deleted = dbNode.isDeleted();
  }

  void addChild(NodeData node) {
    if (deleted) {
      log.warn("Adding deleted child to deleted cached node: parent: {}, child: {} ",
          this.getId(), node.getId());
    }
    node.setParent(this);
    //force add
    children.remove(node);
    children.add(node);
  }

  void delete() {
    // cannot simply avoid deleting children if this is
    // deleted because parent could be loaded after children
    this.deleted = true;
    for (Node child : children) {
      ((NodeData) child).delete();
    }
  }

  NodeData getCurrentRoot() {
    NodeData root = getParent();
    if (root == null) {
      return this;
    }
    while (root.getParent() != null) {
      root = root.getParent();
    }
    return root;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NodeData that = (NodeData) o;
    return id == that.id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
