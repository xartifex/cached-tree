package com.xartifex.task.cached.tree.action;

import com.xartifex.task.cached.tree.cache.TreeCache;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NoopCacheAction implements CacheAction {

  public static NoopCacheAction NOOP = NoopCacheAction.builder().build();

  @Override
  public void perform(TreeCache cache) {
  }
}
