package com.xartifex.task.cached.tree.db;

import com.xartifex.task.cached.tree.cache.NodeData;
import com.xartifex.task.cached.tree.common.IdGenerator;
import com.xartifex.task.cached.tree.common.Node;
import com.xartifex.task.cached.tree.config.SampleDbTreeProvider;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.springframework.stereotype.Component;

@Component
public class DbTreeRepository {

  private final SampleDbTreeProvider sampleDbTreeProvider;
  private final IdGenerator idGenerator;
  private final Map<Integer, DbNode> idToNode = new HashMap<>();
  private DbTree dbTree;

  public DbTreeRepository(
      SampleDbTreeProvider sampleDbTreeProvider,
      IdGenerator idGenerator, DbTree dbTree) {
    this.sampleDbTreeProvider = sampleDbTreeProvider;
    this.idGenerator = idGenerator;
    this.dbTree = dbTree;
    initMap(dbTree.getRoot());
  }

  private void initMap(DbNode root) {
    addToMap(root);
  }

  private void addToMap(DbNode node) {
    idToNode.put(node.getId(), node);
    for (Node child : node.getChildren()) {
      addToMap((DbNode) child);
    }
  }

  public Set<Integer> deleteNode(int nodeId) {
    DbNode nodeToDelete = idToNode.get(nodeId);
    if (nodeToDelete == null) {
      throw new IllegalStateException("Unable to delete non-existing node!" + nodeId);
    }
    idToNode.remove(nodeId);
    return nodeToDelete.delete();
  }

  public NodeData load(int nodeId) {
    DbNode nodeToLoad = idToNode.get(nodeId);
    if (nodeToLoad == null) {
      throw new IllegalStateException("Unable to load non-existing node!");
    }
    return new NodeData(nodeToLoad);
  }

  public void setValue(int nodeId, String value) {
    DbNode nodeToSetValue = idToNode.get(nodeId);
    if (nodeToSetValue == null) {
      throw new IllegalStateException("Unable to set value to non-existing node!");
    }
    nodeToSetValue.setValue(value);
  }

  public Set<Integer> addNode(NodeData newNode) {
    DbNode nodeToAddChildTo = idToNode.get(newNode.getParentId());
    if (nodeToAddChildTo == null) {
      throw new IllegalStateException("Unable to add child to non-existing node!");
    }
    DbNode dbNode = DbNode.builder()
        .id(newNode.getId())
        .value(newNode.getValue()).build();
    Set<Integer> deletedNodeIds = nodeToAddChildTo.addChild(dbNode);
    DbNode shouldBeNull = idToNode.put(dbNode.getId(), dbNode);
    if (shouldBeNull != null) {
      throw new IllegalStateException("Attempt to add existing node twice!");
    }
    return deletedNodeIds;
  }

  public Node getRoot() {
    return dbTree.getRoot();
  }

  public void reset() {
    dbTree = sampleDbTreeProvider.getTree(idGenerator);
    idToNode.clear();
    initMap(dbTree.getRoot());
  }
}
