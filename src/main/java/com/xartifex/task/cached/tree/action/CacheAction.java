package com.xartifex.task.cached.tree.action;

import com.xartifex.task.cached.tree.cache.TreeCache;

public interface CacheAction {

  void perform(TreeCache cache);
}
