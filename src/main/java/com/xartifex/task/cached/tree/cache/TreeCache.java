package com.xartifex.task.cached.tree.cache;

import com.xartifex.task.cached.tree.action.AddNodeDbAction;
import com.xartifex.task.cached.tree.action.DbAction;
import com.xartifex.task.cached.tree.action.DeleteNodeDbAction;
import com.xartifex.task.cached.tree.action.SetNodeValueDbAction;
import com.xartifex.task.cached.tree.common.IdGenerator;
import com.xartifex.task.cached.tree.common.Node;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import org.springframework.stereotype.Component;


@Component
public class TreeCache {

  private final IdGenerator idGenerator;
  private final Queue<DbAction> dbActions = new LinkedList<>();
  private final Map<Integer, NodeData> idToNode = new HashMap<>();
  private final Map<Integer, Set<NodeData>> parentIdToChildren = new HashMap<>();

  public TreeCache(IdGenerator idGenerator) {
    this.idGenerator = idGenerator;
  }

  public void addToCache(NodeData justLoaded) {
    int justLoadedNodeId = justLoaded.getId();
    // check if already loaded and
    // also take into account case when node was recursively deleted while cache didn't have parent of just loaded node
    NodeData alreadyPresent = idToNode.get(justLoadedNodeId);
    if (alreadyPresent != null && ((justLoaded.isDeleted() && alreadyPresent.isDeleted()) || (
        justLoaded.isDeleted() && !alreadyPresent.isDeleted()))) {
      return;
    }
    if (alreadyPresent != null && alreadyPresent.isDeleted()) {
      justLoaded.setDeleted(true);
    }
    //update children if any present and loaded, link them to just loaded parent
    Set<NodeData> children = parentIdToChildren.get(justLoadedNodeId);
    if (children != null) {
      for (NodeData child : children) {
        Set<Node> _children = justLoaded.getChildren();
        //force add
        _children.remove(child);
        _children.add(child);
        child.setParent(justLoaded);
      }
    }
    //set parent if any and loaded
    Integer parentId = justLoaded.getParentId();
    if (parentId != null) {
      NodeData alreadyLoadedParent = idToNode.get(justLoaded.getParentId());
      if (alreadyLoadedParent != null) {
        alreadyLoadedParent.addChild(justLoaded);
        justLoaded.setParent(alreadyLoadedParent);
        if (alreadyLoadedParent.isDeleted()) {
          justLoaded.delete();
        }
      }
      updateParentIdToChildren(parentId, justLoaded);
    }
    idToNode.put(justLoadedNodeId, justLoaded);
  }

  public void markDeleted(int nodeId) {
    NodeData cachedNode = idToNode.get(nodeId);
    if (cachedNode == null) {
      throw new IllegalStateException(
          "Attempt to delete non existing or not loaded node! " + nodeId);
    }
    cachedNode.delete();
    dbActions.add(DeleteNodeDbAction.builder().nodeId(nodeId).build());
  }

  //non-recursive
  public void setDeleted(int nodeId) {
    NodeData nodeData = idToNode.get(nodeId);
    if (nodeData != null) {
      nodeData.setDeleted(true);
    }
  }

  public void addNode(NodeData newNode) {
    Integer parentId = newNode.getParentId();
    if (parentId == null) {
      throw new IllegalStateException("Parent id cannot be null!");
    }
    newNode.setId(idGenerator.getNextId());
    NodeData parent = idToNode.get(parentId);
    if (parent == null) {
      throw new IllegalStateException("Parent was not loaded: " + parentId);
    }
    if (parent.isDeleted()) {
      throw new IllegalStateException("Parent is deleted. Modification is forbidden!");
    }
    parent.addChild(newNode);
    newNode.setParent(parent);

    updateParentIdToChildren(parentId, newNode);

    dbActions.add(AddNodeDbAction.builder().newNode(newNode).build());
    idToNode.put(newNode.getId(), newNode);
  }

  public void setValueToNode(int nodeId, String value) {
    NodeData cachedNode = idToNode.get(nodeId);
    if (cachedNode == null) {
      throw new IllegalStateException(
          "Attempt to set value for non existing or not loaded node!" + value);
    }
    cachedNode.setValue(value);
    dbActions.add(SetNodeValueDbAction.builder().nodeId(nodeId).value(value).build());
  }

  public Set<NodeData> compact() {
    Set<NodeData> nodeData = new LinkedHashSet<>();
    for (NodeData loaded : idToNode.values()) {
      nodeData.add(loaded.getCurrentRoot());
    }
    return nodeData;
  }

  public Queue<DbAction> getActions() {
    return dbActions;
  }

  public void reset() {
    idGenerator.reset();
    dbActions.clear();
    idToNode.clear();
    parentIdToChildren.clear();
  }

  private void updateParentIdToChildren(int parentId, NodeData nodeData) {
    //initialize children set for given parentId if not already
    Set<NodeData> childrenForGivenParent =
        parentIdToChildren.computeIfAbsent(parentId, k -> new LinkedHashSet<>());
    //force add
    childrenForGivenParent.remove(nodeData);
    childrenForGivenParent.add(nodeData);
  }
}
