package com.xartifex.task.cached.tree.common;

import org.springframework.stereotype.Component;

@Component
public class TreeUtil {

  public void print(Tree tree) {
    print(tree.getRoot());
  }

  private void print(Node node) {
    System.out.println("<ul>");
    System.out.println("<li>");
    System.out.println(node.getValue());
    for (Node child : node.getChildren()) {
      print(child);
    }
    System.out.println("</li>");
    System.out.println("</ul>");
  }

  public StringBuilder toHtml(Node root, StringBuilder sb, String classes) {
    sb.append("<ul>");
    sb.append("<li>");
    sb.append("<div class=\"node ").append(classes).append("\"")
        .append(" data-id=\"").append(root.getId()).append("\" ")
        .append(" data-value=\"").append(root.getValue()).append("\"")
        .append(" data-deleted=\"").append(root.isDeleted()).append("\"");
    sb.append(">");
    sb.append(root.getValue())
        .append(" - ").append(root.getId())
        .append(" - ").append(root.isDeleted());
    sb.append("</div>");
    for (Node child : root.getChildren()) {
      toHtml(child, sb, classes);
    }
    sb.append("</li>");
    sb.append("</ul>");
    return sb;
  }
}
