package com.xartifex.task.cached.tree.config;

import com.xartifex.task.cached.tree.common.IdGenerator;
import com.xartifex.task.cached.tree.db.DbNode;
import com.xartifex.task.cached.tree.db.DbTree;
import org.springframework.stereotype.Component;

@Component
public class SampleDbTreeProvider {

  public DbTree getTree(IdGenerator idGenerator) {
    DbNode root = DbNode.builder().id(idGenerator.getNextId()).value("Node1").build();
    DbNode node2 = DbNode.builder().id(idGenerator.getNextId()).value("Node2").build();
    DbNode node3 = DbNode.builder().id(idGenerator.getNextId()).value("Node3").build();
    DbNode node4 = DbNode.builder().id(idGenerator.getNextId()).value("Node4").build();
    DbNode node5 = DbNode.builder().id(idGenerator.getNextId()).value("Node5").build();
    DbNode node6 = DbNode.builder().id(idGenerator.getNextId()).value("Node6").build();
    DbNode node7 = DbNode.builder().id(idGenerator.getNextId()).value("Node7").build();
    DbNode node8 = DbNode.builder().id(idGenerator.getNextId()).value("Node8").build();
    DbNode node9 = DbNode.builder().id(idGenerator.getNextId()).value("Node9").build();
    DbNode node10 = DbNode.builder().id(idGenerator.getNextId()).value("Node10").build();

    root.addChild(node2);
    root.addChild(node3);
    root.addChild(node5);
    node3.addChild(node4);
    node5.addChild(node6);
    node6.addChild(node7);
    node7.addChild(node8);
    node8.addChild(node9);
    node9.addChild(node10);

    return DbTree.builder().root(root).build();
  }
}
