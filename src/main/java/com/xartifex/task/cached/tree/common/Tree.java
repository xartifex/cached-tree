package com.xartifex.task.cached.tree.common;

public interface Tree {

  Node getRoot();
}
