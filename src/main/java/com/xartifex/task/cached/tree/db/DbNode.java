package com.xartifex.task.cached.tree.db;

import com.xartifex.task.cached.tree.common.Node;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;


@Builder
@Getter
@Setter
@Slf4j
public class DbNode implements Node {

  private int id;
  private String value;
  private Node parent;
  @Default
  private Set<Node> children = new LinkedHashSet<>();
  private boolean deleted;

  public Set<Integer> addChild(DbNode node) {
    Set<Integer> deletedNodeIds = new HashSet<>();
    if (deleted) {
      log.warn("Attempt to add deleted child to deleted db node, set it to deleted: parent: {}, child: {} ",
          this.getId(), node.getId());
      deletedNodeIds = node.delete();
    }
    node.setParent(this);
    children.add(node);
    return deletedNodeIds;
  }

  public Set<Integer> delete() {
    Set<Integer> deletedNodeIds = new HashSet<>();
    _delete(deletedNodeIds);
    return deletedNodeIds;
  }

  private void _delete(Set<Integer> deletedNodeIds) {
    if (deleted) {
      return;
    }
    this.deleted = true;
    deletedNodeIds.add(this.getId());
    for (Node child : children) {
      ((DbNode) child)._delete(deletedNodeIds);
    }
  }
}
