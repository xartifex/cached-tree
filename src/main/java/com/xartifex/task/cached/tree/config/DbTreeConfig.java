package com.xartifex.task.cached.tree.config;

import com.xartifex.task.cached.tree.common.IdGenerator;
import com.xartifex.task.cached.tree.db.DbTree;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DbTreeConfig {

  @Bean
  public DbTree dbTree(SampleDbTreeProvider dbTreeProvider, IdGenerator idGenerator) {
    return dbTreeProvider.getTree(idGenerator);
  }

}
