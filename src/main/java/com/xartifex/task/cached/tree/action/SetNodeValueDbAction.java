package com.xartifex.task.cached.tree.action;

import static com.xartifex.task.cached.tree.action.NoopCacheAction.NOOP;

import com.xartifex.task.cached.tree.db.DbTreeRepository;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SetNodeValueDbAction implements DbAction {

  private final int nodeId;
  private final String value;

  @Override
  public CacheAction perform(DbTreeRepository dbTreeRepository) {
    dbTreeRepository.setValue(nodeId, value);
    return NOOP;
  }
}
