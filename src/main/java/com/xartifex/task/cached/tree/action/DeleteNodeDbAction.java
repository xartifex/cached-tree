package com.xartifex.task.cached.tree.action;

import com.xartifex.task.cached.tree.db.DbTreeRepository;
import java.util.Set;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DeleteNodeDbAction implements DbAction {

  private final int nodeId;

  @Override
  public CacheAction perform(DbTreeRepository dbTreeRepository) {
    Set<Integer> deletedNodeIds = dbTreeRepository.deleteNode(nodeId);
    return PostDeleteCacheAction.builder().deletedNodeIds(deletedNodeIds).build();
  }

}
