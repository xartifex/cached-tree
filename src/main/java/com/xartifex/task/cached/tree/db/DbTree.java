package com.xartifex.task.cached.tree.db;

import com.xartifex.task.cached.tree.common.Tree;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DbTree implements Tree {

  private DbNode root;
}
