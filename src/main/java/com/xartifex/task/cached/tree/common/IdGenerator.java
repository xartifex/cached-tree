package com.xartifex.task.cached.tree.common;

import org.springframework.stereotype.Component;

@Component
public class IdGenerator {

  private int id = 1;

  public int getNextId() {
    return id++;
  }

  public void reset() {
    id = 1;
  }
}
