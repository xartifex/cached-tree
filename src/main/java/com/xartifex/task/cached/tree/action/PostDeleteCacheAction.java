package com.xartifex.task.cached.tree.action;

import com.xartifex.task.cached.tree.cache.TreeCache;
import java.util.HashSet;
import java.util.Set;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;

@Data
@Builder
public class PostDeleteCacheAction implements CacheAction {

  @Default
  private final Set<Integer> deletedNodeIds = new HashSet<>();

  @Override
  public void perform(TreeCache cache) {
    for (Integer deletedNodeId : deletedNodeIds) {
      cache.setDeleted(deletedNodeId);
    }
  }
}
