package com.xartifex.task.cached.tree.action;

import com.xartifex.task.cached.tree.cache.NodeData;
import com.xartifex.task.cached.tree.db.DbTreeRepository;
import java.util.Set;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AddNodeDbAction implements DbAction {

  private final NodeData newNode;

  @Override
  public CacheAction perform(DbTreeRepository dbTreeRepository) {
    Set<Integer> deletedNodeIds = dbTreeRepository.addNode(newNode);
    return PostDeleteCacheAction.builder().deletedNodeIds(deletedNodeIds).build();
  }
}
