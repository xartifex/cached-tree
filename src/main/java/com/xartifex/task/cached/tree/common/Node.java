package com.xartifex.task.cached.tree.common;

import java.util.Set;

public interface Node {

  int getId();

  String getValue();

  Node getParent();

  Set<Node> getChildren();

  boolean isDeleted();
}
