package com.xartifex.task.cached.tree.action;

import com.xartifex.task.cached.tree.db.DbTreeRepository;

public interface DbAction {

  CacheAction perform(DbTreeRepository dbTreeRepository);
}
