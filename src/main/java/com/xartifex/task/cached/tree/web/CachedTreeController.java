package com.xartifex.task.cached.tree.web;

import com.xartifex.task.cached.tree.action.CacheAction;
import com.xartifex.task.cached.tree.action.DbAction;
import com.xartifex.task.cached.tree.cache.NodeData;
import com.xartifex.task.cached.tree.cache.TreeCache;
import com.xartifex.task.cached.tree.common.TreeUtil;
import com.xartifex.task.cached.tree.db.DbTreeRepository;
import java.util.HashSet;
import java.util.Set;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/tree")
public class CachedTreeController {

  public static final String CACHED_CLASS = "cached-node";
  public static final String DB_CLASS = "db-node";

  private final TreeUtil treeUtil;
  private final TreeCache treeCache;
  private final DbTreeRepository dbTreeRepository;

  public CachedTreeController(TreeUtil treeUtil,
      TreeCache treeCache,
      DbTreeRepository dbTreeRepository) {
    this.treeUtil = treeUtil;
    this.treeCache = treeCache;
    this.dbTreeRepository = dbTreeRepository;
  }

  @GetMapping("/reset")
  public ResponseEntity<String> reset() {
    treeCache.reset();
    dbTreeRepository.reset();
    return ResponseEntity.ok("OK");
  }

  @GetMapping("/db/load/all")
  @ResponseBody
  public String loadAllDbTree() {
    return treeUtil.toHtml(dbTreeRepository.getRoot(), new StringBuilder(), DB_CLASS).toString();
  }

  @GetMapping("/db/load/{nodeId}")
  @ResponseBody
  public String loadFromDbAndGetUpdatedCachedTree(@PathVariable int nodeId) {
    NodeData nodeData = dbTreeRepository.load(nodeId);
    treeCache.addToCache(nodeData);
    return calculateView();
  }

  @GetMapping("/cache/load/all")
  @ResponseBody
  public String getFullCacheContents() {
    return calculateView();
  }


  @GetMapping("/cache/add")
  @ResponseBody
  public String addNode(@RequestParam int parentId, @RequestParam
      String value) {
    treeCache.addNode(NodeData.builder().parentId(parentId).value(value).build());
    return calculateView();
  }

  @GetMapping("/cache/remove")
  @ResponseBody
  public String deleteNode(@RequestParam int nodeId) {
    treeCache.markDeleted(nodeId);
    return calculateView();
  }

  @GetMapping("/cache/update")
  @ResponseBody
  public String updateNode(@RequestParam int nodeId, @RequestParam String value) {
    treeCache.setValueToNode(nodeId, value);
    return calculateView();
  }

  @GetMapping("/cache/apply")
  public ResponseEntity<String> applyChangesToDb() {
    Set<CacheAction> cacheActions = new HashSet<>();
    for (DbAction dbAction : treeCache.getActions()) {
      cacheActions.add(dbAction.perform(dbTreeRepository));
    }
    for (CacheAction cacheAction : cacheActions) {
      cacheAction.perform(treeCache);
    }
    treeCache.getActions().clear();
    return ResponseEntity.ok("OK");
  }

  private String calculateView() {
    Set<NodeData> compacted = treeCache.compact();
    StringBuilder result = new StringBuilder();
    for (NodeData root : compacted) {
      treeUtil.toHtml(root, result, CACHED_CLASS);
    }
    return result.toString();
  }

}
