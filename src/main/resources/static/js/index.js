var selectedNode;

var buttonsCache;
var buttonsDb;

function initSelection() {
  $(".node").click(function() {
    if (selectedNode) {
      selectedNode.removeClass("selected")
    }
    $(this).addClass("selected");
    selectedNode = $(this);
    console.log(selectedNode.data("id") + " " + selectedNode.attr("class"));
    if (selectedNode.data("deleted")) {
      buttonsCache.prop("disabled", true);
    } else {
      if (selectedNode.hasClass("cached-node")) {
        buttonsCache.prop("disabled", false);
        buttonsDb.prop("disabled", true);
      }
    }

    if (selectedNode.hasClass("db-node")) {
      buttonsCache.prop("disabled", true);
      buttonsDb.prop("disabled", false);
    }

  });
}

function initButtonsVars() {
  buttonsCache = $(".commands-cache").children("button").not(".reset").not(".apply");
  buttonsDb = $(".commands-db").children("button");
}

function resetSelection() {
  if (selectedNode) {
    selectedNode.removeClass("selected");
    selectedNode = null;
  }
  buttonsCache.prop("disabled", true);
  buttonsDb.prop("disabled", true);
}

function loadDbTree() {
  $.ajax({
    url: "/tree/db/load/all",
    success: function(data) {
      resetSelection();
      $(".db-tree").html(data);
      initSelection();
    },
    dataType: "html"
  });
}

function loadCache() {
  $.ajax({
    url: "/tree/cache/load/all",
    success: function(data) {
      resetSelection();
      $(".cached-tree").html(data);
      initSelection();
    },
    dataType: "html"
  });
}

/* Buttons and their functions */
//Add
function addChild(parentId, value) {
  $.ajax({
    url: "/tree/cache/add?" + jQuery.param({
      "parentId": parentId,
      "value": value
    }),
    success: function(data) {
      resetSelection();
      $(".cached-tree").html(data);
      initSelection();
    },
    dataType: "html"
  });
}

function openAddChildDialog(parentId) {
  var popup = $(".modal-template");
  popup.find("label").first().text("Adding child to node with id " + parentId + ". Specify new node value:")
  popup.show();
  var okButton = popup.find("button.ok").first();
  okButton.unbind('click');
  okButton.click(function() {
    var value = popup.find('input[name="value"]').first().val();
    addChild(parentId, value);
    popup.hide();
  });
  var cancelButton = popup.find("button.cancel").first();
  cancelButton.unbind('click');
  cancelButton.click(function() {
    popup.hide();
  });
}

function initAddButton() {
  $(".add").click(function() {
    if (selectedNode && selectedNode.hasClass("cached-node")) {
      openAddChildDialog(selectedNode.data("id"));
    }
  });
}

//Edit
function editNode(nodeId, value) {
  $.ajax({
    url: "/tree/cache/update?" + jQuery.param({
      "nodeId": nodeId,
      "value": value
    }),
    success: function(data) {
      resetSelection();
      $(".cached-tree").html(data);
      initSelection();
    },
    dataType: "html"
  });
}

function openEditNodeDialog(nodeId) {
  var popup = $(".modal-template");
  popup.find("label").first().text("Modifying existing node value with id " + nodeId + ". Specify new node value:")
  popup.show();
  var okButton = popup.find("button.ok").first();
  okButton.unbind('click');
  okButton.click(function() {
    var value = popup.find('input[name="value"]').first().val();
    editNode(nodeId, value);
    popup.hide();
  });
  var cancelButton = popup.find("button.cancel").first();
  cancelButton.unbind('click');
  cancelButton.click(function() {
    popup.hide();
  });
}

function initEditButton() {
  $(".edit").click(function() {
    if (selectedNode && selectedNode.hasClass("cached-node")) {
      openEditNodeDialog(selectedNode.data("id"));
    }
  });
}

//Remove
function removeNode(nodeId) {
  $.ajax({
    url: "/tree/cache/remove?" + jQuery.param({
      "nodeId": nodeId
    }),
    success: function(data) {
      resetSelection();
      $(".cached-tree").html(data);
      initSelection();
    },
    dataType: "html"
  });
}

function initRemoveButton() {
  $(".remove").click(function() {
    if (selectedNode && selectedNode.hasClass("cached-node")) {
      removeNode(selectedNode.data("id"));
    }
  });
}

//Apply
function apply() {
  $.ajax({
    url: "/tree/cache/apply",
    success: function(data) {
      loadDbTree();
      loadCache();
    },
    dataType: "html"
  });
}

function initApplyButton() {
  $(".apply").click(apply);
}

//Reset
function reset() {
  $.ajax({
    url: "/tree/reset",
    success: function(data) {
      loadDbTree();
      loadCache();
    },
    dataType: "html"
  });
}

function initResetButton() {
  $(".reset").click(reset);
}

//Load
function loadIntoCache(nodeId) {
  $.ajax({
    url: "/tree/db/load/" + nodeId,
    success: function(data) {
      resetSelection();
      $(".cached-tree").html(data);
      initSelection();
    },
    dataType: "html"
  });
}

function initLoadButton() {
  $(".load").click(function() {
    if (selectedNode && selectedNode.hasClass("db-node")) {
      loadIntoCache(selectedNode.data("id"));
    }
  });
}

/* */

$(document).ready(function() {
  initButtonsVars();
  loadDbTree();
  loadCache();

  initAddButton();
  initRemoveButton();
  initEditButton();
  initApplyButton();
  initResetButton();

  initLoadButton();
});
