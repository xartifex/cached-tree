package com.xartifex.task.cached.tree.db;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.xartifex.task.cached.tree.common.IdGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class IdGeneratorTest {

  @Autowired
  private IdGenerator idGenerator;

  @Test
  void getNextIdReturns0Then1() {
    idGenerator.reset();
    int id1 = idGenerator.getNextId();
    int id2 = idGenerator.getNextId();
    assertEquals(1, id1);
    assertEquals(2, id2);
  }
}