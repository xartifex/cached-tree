package com.xartifex.task.cached.tree.common;

import com.xartifex.task.cached.tree.db.DbNode;
import com.xartifex.task.cached.tree.db.DbTree;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TreeUtilTest {

  @Autowired
  private TreeUtil treeUtil;

  @Test
  public void printTree() {

    DbNode root = DbNode.builder().id(1).value("root - Beverages").build();
    DbNode level11 = DbNode.builder().id(1).value("level11 - Water").build();
    DbNode level12 = DbNode.builder().id(1).value("level12 - Coffee").build();
    DbNode level13 = DbNode.builder().id(1).value("level13 - Tea").build();
    root.addChild(level11);
    root.addChild(level12);
    root.addChild(level13);

    DbNode level21 = DbNode.builder().id(1).value("level21 - Black Tea").build();
    DbNode level22 = DbNode.builder().id(1).value("level22 - White Tea").build();
    DbNode level23 = DbNode.builder().id(1).value("level23 - Green Tea").build();
    level13.addChild(level21);
    level13.addChild(level22);
    level13.addChild(level23);

    DbNode leve31 = DbNode.builder().id(1).value("level31 - Sencha").build();
    DbNode leve32 = DbNode.builder().id(1).value("level32 - Gyokuro").build();
    DbNode leve33 = DbNode.builder().id(1).value("level33 - Matcha").build();
    DbNode leve34 = DbNode.builder().id(1).value("level33 - Pi Lo Chun").build();

    level23.addChild(leve31);
    level23.addChild(leve32);
    level23.addChild(leve33);
    level23.addChild(leve34);

    Tree tree = DbTree.builder().root(root).build();
    treeUtil.print(tree);
  }
}